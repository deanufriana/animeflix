import React, { Component } from "react";
import {
  Animated,
  Platform,
  StatusBar,
  Text,
  View,
  RefreshControl,
  Image,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  Alert
} from "react-native";
import Star from "react-native-star-view";
import Video from "react-native-video";
const { width } = Dimensions.get("window");

import LinearGradient from "react-native-linear-gradient";
import Shimmer from "./Shimmer";
import CardFavorite from "../components/CardFavorite";
import IconI from "react-native-vector-icons/Ionicons";
import {
  POPULAR,
  ALL_VIDEOS,
  USER,
  DETAIL_VIDEO,
  ACTIVE
} from "../actions/video";


import textStyle from '../styles/textStyle'
import { connect } from "react-redux";
import axios from "axios";
import ip from "../config";
import cheerio from "cheerio-without-node-native";

import IconA from "react-native-vector-icons/FontAwesome";

import { Content, Button, Col, Row, Grid } from "native-base";

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = Platform.OS === "ios" ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

// const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

class Parallax extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        Platform.OS === "ios" ? -HEADER_MAX_HEIGHT : 0
      ),
      refreshing: false,
      token: "",
      isLoading: false,
      isLogin: false,
      videos: 5,
      popular: 6,
      preview: false,
      slug: "",
      offset: 0,
      focus: "",
      data: {}
    };
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem("token");
    this.props.dispatch(POPULAR(this.state.popular));
    this.props.dispatch(ALL_VIDEOS(this.state.offset, this.state.videos));

    if (token) {
      // this.props.dispatch(GET_FAVORIT(token));
      this.setState({ isLogin: true, preview: false });

      const result = axios
        .get(ip + "/user/profile", {
          headers: {
            Authorization: "Bearer " + token
          }
        })
        .then(response => {
          this.props.dispatch(USER(result.data));
        });
    } else {
      this.setState({ isLogin: false });
    }
  }

  user() {
    if (this.state.isLoading) {
      return <ActivityIndicator />;
    } else {
      if (this.state.isLogin) {
        return (
          <IconI
            onPress={this.loginLogout}
            name="ios-contact"
            style={{ fontSize: 35 }}
            color="white"
          />
        );
      } else {
        return (
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("LoginScreen")}
          >
            <View
              style={{
                width: 70,
                height: 30,
                backgroundColor: "#2980b9",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                borderRadius: 10
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 12,
                  fontFamily: "Roboto-Medium",
                  fontWeight: "500"
                }}
              >
                Login
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
    }
  }

  previewVideo() {
    return (
      <View
        style={{
          height: null,
          width,
          marginTop: 5,
          marginRight: 20
        }}
      >
        <Video
          muted={true}
          ref={ref => {
            this.player = ref;
          }}
          paused={this.state.paused}
          source={{ uri: this.state.video }}
          style={textStyle.backgroundVideo}
        />
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={["#000", "rgba(0,0,0,0.7)", "rgba(0,0,0,0.4)"]}
          style={textStyle.linearGradient}
        >
          <Content padder style={{ backgroundColor: "transparent" }}>
            {this.props.allVideo.isLoading ? (
              <ActivityIndicator />
            ) : (
              <Grid>
                <Row>
                  <Col size={5}>
                    <Text
                      style={{
                        fontFamily: "Roboto",
                        fontSize: 20,
                        fontWeight: "bold",
                        color: "#FBFBFB",
                        marginRight: 5
                      }}
                    >
                      {this.props.allVideo.data.series &&
                        this.props.allVideo.data.series.substring(0, 50)}
                    </Text>
                  </Col>

                  {/* <Col size={1}>
                  <IconI onPress={() => this.setState({ preview: false })} name='ios-close' color='#FBFBFB' style={{ flex: 1, alignSelf: 'flex-end', marginRight: 5 }} size={35} />
                </Col> */}
                </Row>
                <Row style={{ width: 230, marginBottom: 15 }}>
                  <Text
                    style={{
                      color: "#7D7D7D",
                      fontSize: 12,
                      textAlign: "justify"
                    }}
                  >
                    {this.props.allVideo.isLoading ? (
                      <ActivityIndicator />
                    ) : (
                      this.props.allVideo.data.description &&
                      this.props.allVideo.data.description.substring(7, 150)
                    )}
                  </Text>
                </Row>
                <Row>
                  <Col style={{ flexDirection: "row" }}>
                    <Button
                      style={{
                        width: null,
                        height: 30,
                        backgroundColor: "#E10916"
                      }}
                      onPress={() =>
                        this.props.navigation.push("VideoScreen", {
                          slug: this.state.data,
                          video: this.state.video,
                          isLogin: this.state.isLogin,
                          series: this.props.allVideo.data.series
                        })
                      }
                    >
                      <IconI
                        size={20}
                        color="#FBFBFB"
                        style={{ marginLeft: 10 }}
                        name="md-play"
                      />

                      <Text
                        style={{
                          color: "#FBFBFB",
                          fontWeight: "bold",
                          padding: 10
                        }}
                      >
                        Play
                      </Text>
                    </Button>
                    <Button
                      style={{
                        width: 70,
                        marginLeft: 5,
                        borderColor: "#FBFBFB",
                        borderWidth: 1,
                        height: 30,
                        backgroundColor: "transparent"
                      }}
                    >
                      <Image
                        source={require("../assets/icon/add.png")}
                        style={{ height: 12, width: 12, marginLeft: 5 }}
                      />
                      <Text style={{ color: "#FBFBFB", fontWeight: "bold" }}>
                        Add
                      </Text>
                      <Text />
                    </Button>
                  </Col>
                </Row>
              </Grid>
            )}
          </Content>
        </LinearGradient>
      </View>
    );
  }

  loginLogout = () => {
    this.setState({ isLoading: true });

    if (this.state.isLogin) {
      Alert.alert(
        "Do you want logout ?",
        "u will dont have any add favorite and update again",
        [
          {
            text: "OK",
            onPress: async () =>
              await AsyncStorage.removeItem("token").then(() => {
                this.props.navigation.push("Parallax"),
                  this.setState({ isLoading: false });
              })
          },
          {
            text: "Cancel",
            onPress: () => this.setState({ isLoading: false }),
            style: "cancel"
          }
        ],
        { cancelable: true }
      );
    } else {
      this.setState({ isLoading: false });
      this.props.navigation.navigate("LoginScreen");
    }
  };

  empty() {
    if (this.props.allVideo.isLoading) {
      return (
        <FlatList
          keyExtractor={index => `index${index}`}
          data={[1, 2, 3]}
          numColumns={3}
          renderItem={() => (
            <Shimmer
              style={{ height: 180, width: 110, marginRight: 10 }}
              autoRun={true}
            />
          )}
        />
      );
    } else if (this.props.allVideo.isError) {
      return <Image source={require("../assets/img/notFound.jpg")} />;
    }
  }

  preview = async series => {
    // data = series.replace(/\s+/g, "-").toLowerCase() + "-episode-1";

    await this.props.dispatch(ACTIVE(series));
    await this.props.dispatch(DETAIL_VIDEO(series));
    axios.get(this.props.allVideo.data.video_url).then(response => {
      const $ = cheerio.load(response.data);
      const video = $("source").attr("src");
      this.setState({ video, data: series });
    });

    this.setState({ preview: true });
  };

  handleMorePopular = () => {
    this.setState({ popular: this.state.popular + 2 });
    this.props.dispatch(POPULAR(this.state.popular + 2));
  };

  footerComponent = () => {
    return (
      <FlatList
        keyExtractor={index => `index${index}`}
        data={[1, 2, 3]}
        numColumns={3}
        renderItem={() => (
          <Shimmer
            style={{ height: 180, width: 110, marginRight: 10 }}
            autoRun={true}
          />
        )}
      />
    );
  };

  newRelease = item => (
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        marginRight: 10
      }}
      onPress={() => this.preview(item.slug)}
    >
      <Content style={{ flex: 1 }}>
        <View
          style={{
            backgroundColor: "transparent",
            borderWidth: 0,
            margin: 5,
            borderColor: "transparent",
            width: null,
            height: 170
          }}
        >
          <ImageBackground
            style={{ width: null, height: 150 }}
            source={{ uri: item.image_url }}
            imageStyle={
              this.props.active.data === item.slug && {
                borderWidth: 2,
                borderColor: "#fff"
              }
            }
          >
            {this.props.active.data === item.slug && (
              <View style={textStyle.talkBubbleTriangle} />
            )}
            <Button
              small
              style={{ margin: 5, backgroundColor: "#E10916", height: 14 }}
            >
              <Text style={{ fontSize: 10, color: "white", margin: 5 }}>
                Episode : {item.episode}
              </Text>
            </Button>
            <View style={{ flex: 1, justifyContent: "flex-end" }}>
              <View
                style={{
                  backgroundColor: "rgba(0,0,0,0.5)",
                  marginHorizontal: 2,
                  marginBottom: 2,
                  height: 40,
                  width: null
                }}
              >
                <View style={{ height: null, width: 110 }}>
                  <Text
                    style={{
                      fontFamily: "Roboto-Medium",
                      fontSize: 12,
                      color: "white",
                      marginLeft: 5
                    }}
                  >
                    {item.series && item.series.substring(0, 30)}
                  </Text>
                </View>

                <Star
                  score={item.rating / 2}
                  style={{ width: 40, height: 8, marginLeft: 5 }}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
      </Content>
    </TouchableOpacity>
  );

  sendState = () => {
    return this.state.focus;
  };

  _renderScrollViewContent() {
    const data = Array.from({ length: 30 });
    return (
      <View style={textStyle.scrollViewContent}>
        <Content padder>
          {this.props.allVideo.isError ? (
            <View>
              <Text style={textStyle.title}>Jaringan Tidak Ditemukan</Text>
              <Image source={require("../assets/img/notFound.jpg")} />
            </View>
          ) : (
            <View>

                <Text style={textStyle.title}>New Release</Text>

              <FlatList
                keyExtractor={item => `index${item.id}`}
                horizontal={true}
                data={this.props.allVideo.results}
                renderItem={({ item }) => this.newRelease(item)}
                ListEmptyComponent={this.empty()}
              />
            </View>
          )}
          {this.state.preview && this.previewVideo()}
        </Content>
        <Content padder>
          {this.props.allVideo.data.isError || (
            <View>
              <View style={{ flexDirection: "row" }}>
                <Text style={textStyle.title}>Most Popular</Text>
              </View>

              <FlatList
                numColumns={3}
                keyExtractor={(item, index) => `index${index}`}
                data={this.props.popular.results}
                showsHorizontalScrollIndicator={false}
                renderPlaceholder
                renderItem={({ item }) => (
                  <CardFavorite item={item} {...this.props} />
                )}
              />
            </View>
          )}
        </Content>
      </View>
    );
  }

  render() {
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === "ios" ? HEADER_MAX_HEIGHT : 0
    );

    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: "clamp"
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: "clamp"
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 150],
      extrapolate: "extend"
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: "clamp"
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: "clamp"
    });

    return (
      <View style={textStyle.fill}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Animated.ScrollView
          style={textStyle.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true }
          )}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                setTimeout(() => this.setState({ refreshing: false }), 1000);
              }}
              // Android offset for RefreshControl
              progressViewOffset={HEADER_MAX_HEIGHT}
            />
          }
          // iOS offset for RefreshControl
          contentInset={{
            top: HEADER_MAX_HEIGHT
          }}
          contentOffset={{
            y: -HEADER_MAX_HEIGHT
          }}
        >
          {this._renderScrollViewContent()}
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="none"
          style={[
            textStyle.header,
            { transform: [{ translateY: headerTranslate }] }
          ]}
        >
          <Animated.View
            style={[
              textStyle.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }]
              }
            ]}
          >
            <TouchableOpacity onPress={() => alert("play")}>
              <ImageBackground
                source={require("../assets/img/anime.jpg")}
                style={{ height: 250 }}
              >
                <View
                  style={{
                    backgroundColor: "rgba(0,0,0,0.7)",
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontFamily: "Roboto",
                        fontSize: 20,
                        fontWeight: "bold",
                        color: "#FBFBFB"
                      }}
                    >
                      Shokugeki no Sōma
                    </Text>
                  </View>
                  <View>
                    <Star score={4} style={{ width: 60, height: 12 }} />
                  </View>
                  <View style={{ width: 300, height: 20, marginTop: 20 }}>
                    <Text
                      style={{
                        color: "#7D7D7D",
                        fontSize: 12,
                        textAlign: "center"
                      }}
                    >
                      Impian dari Yukihira Souma adalah menjadi seorang koki
                      fulltime di restoran ayahnya dan melampaui keterampilan
                      memasak ayahnya.{" "}
                    </Text>
                  </View>
                </View>
              </ImageBackground>
              <View
                style={{
                  height: 35,
                  alignSelf: "center",
                  flexDirection: "row",
                  marginTop: -20
                }}
              >
                <Button
                  style={{
                    width: null,
                    height: 30,
                    backgroundColor: "#E10916"
                  }}
                  onPress={() =>
                    this.props.navigation.push("VideoScreen", {
                      series: this.props.allVideo.data.series
                    })
                  }
                >
                  <IconI
                    size={20}
                    color="#FBFBFB"
                    style={{ marginLeft: 10 }}
                    name="md-play"
                  />

                  <Text
                    style={{
                      color: "#FBFBFB",
                      fontWeight: "bold",
                      padding: 10
                    }}
                  >
                    Play
                  </Text>
                </Button>

                <Button
                  style={{
                    width: 70,
                    marginLeft: 5,
                    borderColor: "#FBFBFB",
                    borderWidth: 1,
                    height: 30,
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    source={require("../assets/icon/add.png")}
                    style={{ height: 12, width: 12, marginLeft: 5 }}
                  />
                  <Text style={{ color: "#FBFBFB", fontWeight: "bold" }}>
                    Add
                  </Text>
                  <Text />
                </Button>
              </View>
            </TouchableOpacity>
          </Animated.View>
        </Animated.View>
        <Animated.View style={[textStyle.bar, { alignItems: "center" }]}>
          {/* <Text style={textStyle.title}>Title</Text> */}
          <View style={{ width: "100%", height: 60, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Image
                style={{ width: 110, height: 30, marginLeft: 10 }}
                source={require("../assets/img/Animeflix(EDITED).png")}
              />
            </View>
            <View
              style={{ justifyContent: "center", height: 40, marginRight: 10 }}
            >
              <View
                style={{
                  alignSelf: "flex-end",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <IconA
                  onPress={() => this.props.navigation.navigate("SearchScreen")}
                  name="search"
                  style={{ fontSize: 20, marginRight: 20 }}
                  color="white"
                  onPress={() => this.props.navigation.navigate("SearchScreen")}
                />
                <IconA
                  name="bell"
                  style={{ fontSize: 20, marginRight: 20 }}
                  color="white"
                />

                {this.user()}
              </View>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

const stateMapToProps = state => ({
  popular: state.popularReducers,
  allVideo: state.videoReducers,
  user: state.userReducers,
  favorite: state.favoriteReducers,
  active: state.activeReducers
});

export default connect(stateMapToProps)(Parallax);
